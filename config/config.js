const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {

  development: {
    root: rootPath,
    app: {
      name: 'entradasimples'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/fw'
  },

  test: {
    root: rootPath,
    app: {
      name: 'entradasimples'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/fw'
  },

  production: {
    root: rootPath,
    app: {
      name: 'entradasimples'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/fw'
  }
};

module.exports = config[env];
