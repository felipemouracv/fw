const path = require('path');
const ROOT_PATH = path.resolve(__dirname);
const constants = {
  ROOT_CONFIG: {
    ROOT_DIRECTORY: ROOT_PATH
  },
  AUTH: {
    TIME_TOKEN_TTL: 3600, //em segundos,
    SECRET_KEY: 'fw_secret'
  },
  MODEL: {
    SOFT_DELETE: true
  },
  QRCODE: {
    QUALITY: 'H',
    FOLDER_QR_CODES: ROOT_PATH + '/../qr_codes/',
    UPLOAD_AWS: true,
    DEFAULT_EXTENSION: 'png'
  },
  AWS: {
    AWS_CREDENTIALS: {
      "accessKeyId": "fw",
      "secretAccessKey": "fw",
      "region": "us-east-1"
    },
    SES: {
      SOURCE: 'fw'
    },
    S3: {
      BUCKET_QRCODE: 'fw'
    }
  },
  ENTRADA_SIMPLES: {
    TICKET_NOMINAL: false,
    SERVER_NAME: 'fw',
    VERSION: '1.0.0'
  },
};

module.exports = constants;
