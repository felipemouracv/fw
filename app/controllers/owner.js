const _ = require('lodash');
const Owner = require('../models/owner');
const OwnerService = require('../services/owner');

//owner é diferenciado
const OwnerController = {

  _login: async (req, res) => {

    try {
      let tokenOwner = await OwnerService.validateLogin(req.body.username, req.body.password);
      res.json(tokenOwner);
    } catch (e) {
      res.status(401).json(e);
    }

  },

  _index: async (req, res) => {

    let owner = await Owner.find({_id: req.owner._id});
    res.json(owner);

  },

  _save: async (req, res) => {
    try {

      let data = req.body;
      let newOwner = new Owner(data);
      let owner = await newOwner.save();

      res.json(_.pick(owner, ['_id', 'username', 'email']));
    } catch (e) {
      res.status(400).json(e);
    }
  },

  _update: async (req, res) => {

    try {

      let data = req.body;
      let id = req.params.id;
      let ownerUpdated = await OwnerService._updateOwner(data, id, req.user);
      res.json(ownerUpdated);

    } catch (e) {

      res.status(400).json(e);

    }

  },

  _remove: async (req, res) => {
    try {

      let id = req.params.id;
      if(id !== req.owner._id)
        return res.status(400).json({message: "It is not possible to delete this owner!"});
      let ownerUpdated = await Owner.remove(id);
      res.json({message: 'Removed ok!', ownerUpdated});

    } catch (e) {
      res.status(400).json(e);
    }
  }
};

module.exports = OwnerController;
