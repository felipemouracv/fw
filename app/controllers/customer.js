const _ = require('lodash');
const BaseController = require('./base_controller');
const Customer = require('../models/customer');
const ResponseService = require('./../services/response');

class CustomerController extends BaseController {
  constructor() {
    super(Customer);
    this.updateCustomer = this.updateCustomer.bind(this);
    this.removeCustomer = this.removeCustomer.bind(this);
    this.saveCustomer = this.saveCustomer.bind(this);
  }

  async listCustomer(req, res) {
    let customerData = await Customer.find({owner_id: req.owner._id});
    return ResponseService(res, null, customerData);
  }

  async updateCustomer(req, res) {
    let customer_id = req.params.id;
    let customerData = await Customer.find({_id: customer_id});

    if (!customerData || customerData.owner_id !== req.owner._id)
      return ResponseService(res, null, null, 400, "Customer not found!");

    req.body.owner_id = req.owner._id;
    return this._update(req, res);
  }

  async removeCustomer(req, res) {
    let customer_id = req.params.id;
    let customerData = await Customer.find({_id: customer_id});

    if (!customerData || customerData.owner_id !== req.owner._id)
      return ResponseService(res, null, null, 400, "Customer not found!");

    return this._remove(req, res);
  }

  async saveCustomer(req, res) {
    req.body.owner_id = req.owner._id;
    return this._save(req, res);
  }

}

module.exports = CustomerController;
