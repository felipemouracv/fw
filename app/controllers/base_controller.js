const _ = require('lodash');
const ResponseService = require('./../services/response');

class BaseController {

  constructor(model) {

    this.Model = model;

    this._index = this._index.bind(this);
    this._save = this._save.bind(this);
    this._update = this._update.bind(this);
    this._remove = this._remove.bind(this);

  }

  async _index(req, res) {
    let data = await this.Model.find({});
    return ResponseService(res, null, data);
  }

  async _save(req, res) {
    try {

      let body = req.body;
      let newData = new this.Model(body);
      let data = await newData.save();
      return ResponseService(res, null, data);
    } catch (err) {
      console.warn("ERROR CONTROLLER SAVE: ",e);
      return ResponseService(res, err, null, 400);
    }
  }

  async _update(req, res) {
    try {

      let body = req.body;
      let id = req.params.id;

      await this.Model.update({_id: id}, body);
      return ResponseService(res, null, body);
    } catch (err) {
      console.warn("ERROR CONTROLLER UPDATE: ",e);
      return ResponseService(res, err, null, 400);
    }
  }

  async _remove(req, res) {
    try {

      let id = req.params.id;
      let data = await this.Model.remove({_id: id});

      return ResponseService(res, null, data);
    } catch (err) {
      console.warn("ERROR CONTROLLER REMOVE: ",e);
      return ResponseService(res, err, null, 400);
    }
  }

};

module.exports = BaseController;
