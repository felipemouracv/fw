const express = require('express');
const router = express.Router();
const CustomerController = require('../controllers/customer');
const Auth = require('../middlewares/auth3');

module.exports = (app) => {
  app.use('/customer', router);
};

const Customer = new CustomerController();

router.get('/', Auth.validate, Customer.listCustomer);
router.post('/', Auth.validate, Customer.saveCustomer);
router.put('/:id', Auth.validate, Customer.updateCustomer);
router.delete('/:id', Auth.validate, Customer.removeCustomer);
