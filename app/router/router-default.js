const express = require('express');
const CONSTANTS = require('./../../config/constants');
const router = express.Router();

module.exports = (app) => {
  app.use('/', router);
};

router.get('/', (req, res) => {
  res.json({name: CONSTANTS.ENTRADA_SIMPLES.SERVER_NAME, version: CONSTANTS.ENTRADA_SIMPLES.VERSION});
});
