const express = require('express');
const router = express.Router();
const OwnerController = require('../controllers/owner');
const Auth = require('../middlewares/auth3');

module.exports = (app) => {
  app.use('/owner', router);
};

router.get('/', Auth.validate, OwnerController._index);
router.post('/', OwnerController._save);
router.post('/login', OwnerController._login);
router.put('/:id', Auth.validate, OwnerController._update);
router.delete('/:id', Auth.validate, OwnerController._remove);


