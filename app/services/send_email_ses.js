const _ = require('lodash');
const CONSTANTS = require('../../config/constants');
const fs = require('fs');
const aws = require("aws-sdk");
const Handlebars = require('handlebars');


const SendEmailSES = {

  _sendEmail: async (subject, to = null, emailText = null) => {

    const ses = new aws.SES(CONSTANTS.AWS.AWS_CREDENTIALS);
    const eparam = {
      Destination: {
          ToAddresses: [to]
      },
      Message: {
        Body: {
          Html: {
            Data: emailText
          }
        },
        Subject: {
          Data: subject
        }
      },
      Source: CONSTANTS.AWS.SES.SOURCE,
    };

    ses.sendEmail(eparam, function (err, data) {
      if (err) console.log(new Date(), err, to, subject);
      else console.log(new Date(), data, to, subject);
    });
  },

  _getTemplate: (template_name, objectTemplate) => {
    let source = fs.readFileSync(CONSTANTS.ROOT_CONFIG.ROOT_DIRECTORY+"/../template_emails/"+template_name+'.html', "utf8");
    let template = Handlebars.compile(source);
    return template(objectTemplate);
  }
};

module.exports = SendEmailSES;
