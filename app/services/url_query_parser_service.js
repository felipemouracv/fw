const _ = require('lodash');
const mongoose = require('mongoose');


const listOfOperators = [
  "$eq",	//Matches values that are equal to a specified value.
  "$gt",	//Matches values that are greater than a specified value.
  "$gte",//Matches values that are greater than or equal to a specified value.
  "$in",	//Matches any of the values specified in an array.
  "$lt",	//Matches values that are less than a specified value.
  "$lte",//Matches values that are less than or equal to a specified value.
  "$ne",	//Matches all values that are not equal to a specified value.
  "$nin",//Matches none of the values specified in an array.
];

const UrlQueryParserService = {

  parseToMongoQuery: (params) => {
    let mongoQuery = params;
    _.forOwn(mongoQuery, (value, key) => {

      let operatorOne = "$" + value.substring(0, 2);
      let operatorTwo = "$" + value.substring(0, 3);

      let operator = _.indexOf(listOfOperators, operatorOne) >= 0 ? operatorOne : null;
      operator = _.indexOf(listOfOperators, operatorTwo) >= 0 ? operatorTwo : operator;

      if (operator) {

        let newValue = value.substring(operator.length-1);
        newValue = mongoose.Types.ObjectId.isValid(newValue) ? mongoose.Types.ObjectId(newValue) : newValue;
        newValue = !isNaN(Date.parse(newValue)) ? new Date(newValue) : newValue;

        mongoQuery[key] = {[operator]: newValue};

      }
    });

    return mongoQuery;
  },
};
module.exports = UrlQueryParserService;
