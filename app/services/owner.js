const Owner = require('../models/owner');
const _ = require('lodash');
const CryptoJS = require('crypto-js');
const ThrowerService = require('./thrower');
const CONSTANTS = require('../../config/constants');

const OwnerService = {

  validateToken: async (token) => {

    token = token.replace('Bearer ', '');

    let ownerToken = await Owner.findOne({token: token});

    if (!ownerToken) throw ThrowerService("TOKEN not found!");

    //até aqui blz, mas vamos validar o tempo do token

    let now = new Date().getTime();

    let last = new Date(ownerToken.acess_owner.acess_date).getTime() + (CONSTANTS.AUTH.TIME_TOKEN_TTL * 1000);

    if (last < now)
      throw ThrowerService("Token expired!");

    return ownerToken;
  },

  validateLogin: async (username, password) => {

    let ownerTest = await Owner.findOne({username: username});

    if (!ownerTest) throw ThrowerService("User not found!");

    let passwordHashTest = CryptoJS.AES.encrypt(String(password), ownerTest.secret_key);

    let decryptPasswordTest = CryptoJS.AES.decrypt(passwordHashTest.toString(), ownerTest.secret_key);

    let decryptPassword = CryptoJS.AES.decrypt(String(ownerTest.password), ownerTest.secret_key);

    if (decryptPasswordTest.toString(CryptoJS.enc.Utf8) !== decryptPassword.toString(CryptoJS.enc.Utf8))
      throw ThrowerService("Invalid account!");

    ownerTest.token = OwnerService.generateToken(ownerTest.email, ownerTest.username);

    ownerTest.acess_owner = ownerTest.acess_owner || {};
    ownerTest.password = password;

    ownerTest.acess_owner = {
      acess_date: new Date(),
      token: ownerTest.token
    };

    await ownerTest.save();

    return _.pick(ownerTest, ['_id', 'username', 'email', 'token']);
  },

  generateToken: (email, username) => {
    return CryptoJS.AES.encrypt(
      email + ":" +
      username + ":" +
      (new Date().getTime() + (CONSTANTS.AUTH.TIME_TOKEN_TTL * 1000)), CONSTANTS.AUTH.SECRET_KEY
    ).toString()
  },

  _updateOwner: async (params, _id, owner) => {

    let updateOwner = await Owner.findOne({_id});

    if (!updateOwner)
      throw ThrowerService('Owner not found!');

    if (owner._id !== _id)
      throw ThrowerService('Owner not found!');

    return await Owner.update({_id: _id}, params);

  }
};

module.exports = OwnerService;
