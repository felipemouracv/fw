const _ = require('lodash');
const Event = require('../models/event');
const Ticket = require('../models/ticket');
const ThrowerService = require('./thrower');
const Member = require('../models/member');
const QrCodeService = require('./qrcode');
const SendEmailSES = require('../services/send_email_ses');
const shortId = require('shortid');
const CONSTANT = require('./../../config/constants');
const PromiseBuebird = require('bluebird');

const TicketService = {

  _generateTicket: async (data) => {

    if (!data.event_code)
      throw ThrowerService('Event not found!');

    if (!data.customer_code)
      throw ThrowerService('Member not found!');

    const [event, member, ticketCheck] = await Promise.all([
        Event.findOne({event_code: data.event_code}),
        Member.findOne({customer_code: data.customer_code}).sort({created_at: -1}),
        Ticket.findOne(data)
      ]
    );

    if (!event)
      throw ThrowerService('Event not found!');

    //@todo: Rever para quando for muitos members para um customer
    if (!member)
      throw ThrowerService('Member not found!');

    if (ticketCheck && CONSTANT.ENTRADA_SIMPLES.TICKET_NOMINAL)
      throw ThrowerService('Member has joined in this event!');

    shortId.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');
    let hash = shortId.generate();

    let qrpath = await QrCodeService.generateQrCode(hash, member.username + "_" + hash);

    const ticket = new Ticket({
      member_id: member._id,
      event_id: event._id,
      customer_code: data.customer_code,
      event_code: data.event_code,
      owner_id: data.owner_id,
      qrcode: hash,
      qrcode_img: qrpath.Location
    });

    return await Promise.all(
      [
        SendEmailSES._sendEmail(
          'Confirmação de Ingresso',
          member.email,
          SendEmailSES._getTemplate('confirm_subscription_event', {ticket, member, event})
        ),
        ticket.save()
      ]
    );

  },

  _getTicketsByEvents: async (event_code, owner_id) => {
    const objectFind = {event_code: event_code, owner_id: owner_id};

    const tickets = await Ticket.aggregate([
      {$match: objectFind},
      {
        $lookup:
          {
            from: 'members',
            localField: 'customer_code',
            foreignField: 'customer_code',
            as: 'member'
          }
      }
    ]);

    if (!tickets || _.isEmpty(tickets))
      throw ThrowerService('Not found tickets for this event!');

    return tickets;
  },

  _finalizeTicket: async (ticket_id, date_finalized = null) => {

    const finalizeTicket = await Ticket.findOne({_id: ticket_id});

    if (!finalizeTicket)
      return ThrowerService('Ticket not found!');

    if (!finalizeTicket.is_valid)
      return ThrowerService('Ticket has finalized');


    try {
      finalizeTicket.is_valid = false;
      finalizeTicket.date_finalized = date_finalized || new Date();
      return await finalizeTicket.save();
    } catch (e) {
      console.log("ERRRR: ", e);
      return ThrowerService(e);
    }
  },

  _finalizeEvent: async (event_id, tickets) => {
    const finalizeEvent = await Event.findOne({_id: event_id});

    if (!finalizeEvent)
      throw ThrowerService('Event not found!');

    try {

      let mapPromise = [];
      _.map(tickets, (ticket) => {
        mapPromise.push(TicketService._finalizeTicket(ticket._id, (ticket.date_finalized || null)));
      });

      const resolvePromise = await PromiseBuebird.all(mapPromise);
      finalizeEvent.date_finalized = new Date();
      finalizeEvent.is_finalized = true;
      finalizeEvent.save();
      return resolvePromise;
    } catch (e) {
      throw e;
    }

  }
};


module.exports = TicketService;
