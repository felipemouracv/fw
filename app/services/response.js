const _ = require('lodash');

const ResponseService = (res, err, data, status = null, message = null) => {
  if (err) {
    return res.status(400).json({
      message: err.message || message,
      code: 400,
      status: 400
    });
  }
  status = status || 200;

  let objectReturn = _.extend({}, {data: data});

  if (status)
    objectReturn = _.extend(objectReturn, {code: status, status: status});

  if(message)
    objectReturn = _.extend(objectReturn, {message});

  return res.status(status).json(objectReturn);
};

module.exports = ResponseService;
