const _ = require('lodash');
const CONSTANTS = require('../../config/constants');
const fs = require('fs');
const qr = require('qr-image');
const slugify = require('slugify');
const aws = require("aws-sdk");


const QrCodeService = {

  generateQrCode: async (hash, file_name) => {

    try {
      aws.config.loadFromPath('./config/aws.json');
      const s3 = new aws.S3();

      const code = qr.image(hash, {
        ec_level: CONSTANTS.QRCODE.QUALITY,
        type: CONSTANTS.QRCODE.DEFAULT_EXTENSION,
        size: 15
      });

      if (!fs.existsSync(CONSTANTS.QRCODE.FOLDER_QR_CODES)) {
        fs.mkdirSync(CONSTANTS.QRCODE.FOLDER_QR_CODES);
      }

      file_name = slugify(file_name + '.' + CONSTANTS.QRCODE.DEFAULT_EXTENSION).toLowerCase();
      // const file_path = CONSTANTS.QRCODE.FOLDER_QR_CODES + file_name;

      return await s3.upload({
        Bucket: CONSTANTS.AWS.S3.BUCKET_QRCODE,
        Key: file_name,
        Body: code,
        ACL: 'public-read',
      }).promise();

    } catch (e) {
      return e;
    }
  }

};
module.exports = QrCodeService;
