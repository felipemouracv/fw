const _ = require('lodash');
const ThrowerService = require('./thrower');
const UrlQueryParserService = require('./url_query_parser_service');
const Event = require('./../models/event');

const EventService = {

  _filterEvents: async (params, owner_id) => {
    let filter = UrlQueryParserService.parseToMongoQuery(params);
    filter = _.extend({owner_id}, filter);
    return await Event.find(filter);
  },

};
module.exports = EventService;
