const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const BaseModel = require('./base_model');
const Member = require('./member');
const _ = require('lodash');

const CustomerDocuments = new Schema({
  name: {
    type: String, enum: ['CPF', 'RG'], required: true, validate: [(name) => {
      return name === 'CPF' || name === 'RG'
    }, 'Plase, use at least one valid document!  ']
  },
  document_number: {type: String, required: true},
}, {_id: false});

const CustomerSchema = new Schema({
  username: {type: String, required: true},
  email: {type: String, required: true},
  name: {
    full: {type: String},
    last: {type: String},
    first: {type: String},
  },
  user_documents: {
    type: [CustomerDocuments], required: true, validate: [(arr) => {
      return arr.length > 0;
    }, 'user_documents is required!']
  },
  created_at: {type: Date},
  is_active: {type: Boolean, default: true},
  customer_code: {type: String, required: true},
  owner_id: {type: Schema.Types.ObjectId, ref: 'owner'},
});

mongoose.Promise = require('bluebird');

CustomerSchema.pre('save', async function (next) {
  this.created_at = new Date();
  next();
});

CustomerSchema.post('save', async function (doc) {
  let memberDoc = doc;

  let customer_id = memberDoc._id;
  let objectSave = _.pick(memberDoc, ['email', 'username', 'user_documents', 'customer_code']);
  objectSave.customer_id = customer_id;

  const memberSave = new Member(objectSave);
  await memberSave.save();
});

CustomerSchema.loadClass(BaseModel);

const Customer = mongoose.model('Customer', CustomerSchema);

module.exports = Customer;
