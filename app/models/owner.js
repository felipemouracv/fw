const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CryptoJS = require('crypto-js');
const BaseModel = require('./base_model');
const _ = require('lodash');


const OwnerDocuments = new Schema({
  name: {type: String},
  document_number: {type: String},
}, {id: false});

const AcessOwner = new Schema({
  acess_date: {type: Date},
  token: {type: String},
  is_valid: {type: Boolean, default: true},
  date_invalidate: {type: Date}
}, {id: false});

//TODO mover a coleção acess_owner para uma coleção pai, performace
let OwnerSchema = new Schema({
  username: {type: String, required: true},
  email: {type: String, required: true},
  password: {type: String, required: true},
  secret_key: {type: String},
  name: {
    full: {type: String},
    last: {type: String},
    first: {type: String},
  },
  user_documents: [OwnerDocuments],
  created_at: {type: Date},
  acess_owner: AcessOwner,
  token: {type: String},
  is_active: {type: Boolean, default: true},
  company_name: {type: String, require: true}
});

mongoose.Promise = require('bluebird');

OwnerSchema.pre('save', async function (next) {

  this.secret_key = CryptoJS.SHA512("_" + new Date().getTime() + "_").toString();
  const cipher = CryptoJS.AES.encrypt(this.password, this.secret_key);
  this.created_at = new Date();
  this.password = cipher.toString();
  next();

});
OwnerSchema.loadClass(BaseModel);

const Owner = mongoose.model('Owner', OwnerSchema);


module.exports = Owner;
