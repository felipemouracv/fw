const OwnerService = require('../services/owner');

module.exports = {
  validate: async (req, res, next) => {

    const authorization = req.header('authorization');

    if (!authorization)
      return res.status(401).json({message: "Token invalid!"});

    try {
      const userLogged = await OwnerService.validateToken(authorization);

      if (userLogged) {
        req.owner = userLogged;
        return next();
      }

    } catch (e) {
      return res.status(401).json(e);
    }

    next();
  }
};
